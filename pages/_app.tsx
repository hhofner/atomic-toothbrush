import "../styles/globals.css";
import type { AppProps } from "next/app";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <meta name="viewport" content="width=device-width, user-scalable=no" />
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
