import type { NextPage } from "next";
import Habit, { HabitType } from "../components/habit";
import NewHabitButton from "../components/newHabitButton";
import useHabits from "../hooks/useHabits";
import { main, mainHeader } from "../styles/globals.css";
import { useEffect } from "react";

const Home: NextPage = () => {
  const { all, update, create, complete, delHabit } = useHabits();

  useEffect(() => {
    update(all);
  }, []);

  return (
    <div className={main}>
      <h2 className={mainHeader}>Conquer It</h2>
      {all.map((habit: HabitType) => {
        return (
          <Habit
            key={habit.title}
            title={habit.title}
            history={habit.history}
            streakIndicator={habit.streakIndicator}
            complete={habit.complete}
            handleClick={() => complete(habit)}
            handleDelete={() => delHabit(habit)}
          />
        );
      })}
      <NewHabitButton handleClick={(title) => create(title)} />
    </div>
  );
};

export default Home;
