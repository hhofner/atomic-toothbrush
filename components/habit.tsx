import * as styles from "./habit.css";

export type StreakIndicators =
  | "😶"
  | "😀"
  | "🤠"
  | "🤩"
  | "🔥"
  | "🔥🔥"
  | "🔥🔥🔥";

export enum StreakInd {
  "😶",
  "😀",
  "🤠",
  "🤩",
  "🔥",
  "🔥🔥",
  "🔥🔥🔥",
}

export interface HabitType {
  id: string;
  title: string;
  history: { date: string; complete: boolean }[];
  streakIndicator: StreakIndicators;
  complete: boolean;
}

export interface HabitProps extends Omit<HabitType, "id"> {
  handleClick: () => void;
  handleDelete: () => void;
}

const Habit = ({
  title,
  history,
  streakIndicator,
  complete,
  handleClick,
  handleDelete,
}: HabitProps) => {
  return (
    <div>
      <div
        className={`${styles.container} ${
          complete && styles.containerComplete
        }`}
        onClick={handleClick}
      >
        <h2 className={styles.header}>{title}</h2>
        <div>{history.map((streak) => (streak.complete ? "✅" : "❌"))}</div>
        <div>{streakIndicator}</div>
      </div>
      <small onClick={handleDelete} style={{ cursor: "pointer" }}>
        Delete
      </small>
    </div>
  );
};

export default Habit;
