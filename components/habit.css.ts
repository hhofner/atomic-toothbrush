import { keyframes, style } from "@vanilla-extract/css";

const rise = keyframes({
  "0%": { opacity: "0%", transform: "translateY(-10%)" },
  "100%": { opacity: "100%" },
});

export const container = style({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "baseline",
  cursor: "pointer",
  padding: "1rem",
  border: "2px solid transparent",
  borderRadius: "5px",
  animation: `0.3s ${rise}`,
  ":hover": {
    background: "#007EA7",
    border: "2px solid #003459",
    borderRadius: "5px",
    opacity: 0.8,
  },
});

export const containerComplete = style([
  {
    opacity: "35%",
  },
]);

export const header = style({
  flexBasis: "30%",
});
