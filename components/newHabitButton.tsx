import { useState } from "react";

const NewHabitButton = ({handleClick}: {handleClick: (text: string) => void}) => {
  const [habitText, setHabitText] = useState("");
  return (
    <div>
      <input
        type="text"
        value={habitText}
        onChange={(e) => setHabitText(e.target.value)}
        id="text"
      />
      <button onClick={() => handleClick(habitText)}>add</button>
    </div>
  );
};

export default NewHabitButton;
