### Resources

These are some resources I felt were very helpful:
* [Introduction to Vanilla Extract library. Sweet, isn’t it?](https://tsh.io/blog/vanilla-extract-library/)
* [Theming a React Application with Vanilla Extract](https://formidable.com/blog/2021/vanilla-extract/)