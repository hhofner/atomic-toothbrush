import {
  globalStyle,
  globalFontFace,
  createGlobalTheme,
  style,
} from "@vanilla-extract/css";

globalFontFace("MyGlobalFont", {
  src: 'local("Arial")',
});

globalStyle("*", {
  margin: 0,
  fontFamily: "MyGlobalFont",
});

export const vars = createGlobalTheme(":root", {
  layout: {
    maxWidth: "500px",
  },
});

export const main = style({
  maxWidth: vars.layout.maxWidth,
  margin: "0 auto",
  display: "flex",
  flexDirection: "column",
  gap: "3rem",
});

export const mainHeader = style({
  textAlign: "center",
});
