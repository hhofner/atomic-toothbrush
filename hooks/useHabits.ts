import { useState } from "react";
import { HabitType, StreakInd } from "../components/habit";
import { uuid } from "uuidv4";

type HabitsType = HabitType[];

function useHabits() {
  const key = "habits";
  const [localHabits, setLocalHabits] = useState(() => {
    if (typeof window === "undefined") {
      return [];
    }

    try {
      // Get from local storage by key
      const item = window.localStorage.getItem(key);
      // Parse stored json or if none return initialValue
      return item ? JSON.parse(item) : [];
    } catch (error) {
      // If error also return initialValue
      return [];
    }
  });

  //TODO: Fix bug, create entry for every
  //date (if there's a date missing in between, it won't get filled in)
  const updateHabits = (habits: HabitsType) => {
    const currentDate = new Date();
    setHabits([
      ...habits.map((habit) => {
        const lastRecorded = new Date(
          habit.history[habit.history.length - 1].date
        );
        // Number of days since lastRecorded
        const daysSince = Math.floor(
          (currentDate.getTime() - lastRecorded.getTime()) /
            (1000 * 60 * 60 * 24)
        );
        const streak = habit.history.reduce((p, c) => {
          if (c.complete) {
            //TODO: Fix this, there's only 7 streaks
            if (p + 1 > 6) {
              return 6;
            } else {
              return p + 1;
            }
          } else {
            return 0;
          }
        }, 0);

        let newHistorical = [];
        for (let i = daysSince; i > 0; i--) {
          const newDate = new Date();
          newDate.setDate(newDate.getDate() - (i + 1));
          newHistorical.push({
            date: newDate.toDateString(),
            complete: false,
          });
        }

        return {
          ...habit,
          history: [
            ...habit.history.filter((history) => Boolean(history)),
            ...newHistorical,
          ],
          complete: daysSince > 0 ? false : habit.complete,
          streak: daysSince > 0 ? StreakInd[streak] : habit.streakIndicator,
        };
      }),
    ]);
  };

  const setHabits = (value: HabitsType) => {
    try {
      // Allow value to be a function so we have same API as useState
      const habitsToStore =
        value instanceof Function ? value(localHabits) : value;
      // Save state
      setLocalHabits(habitsToStore);
      // Save to local storage
      if (typeof window !== "undefined") {
        window.localStorage.setItem(key, JSON.stringify(habitsToStore));
      }
    } catch (error) {
      // A more advanced implementation would handle the error case
      console.log(error);
    }
  };

  const addNewHabit = (title: string) => {
    setHabits([
      ...localHabits,
      {
        id: uuid(),
        title: title,
        history: [{ date: new Date().toDateString(), complete: false }],
        streakIndicator: "😶",
        complete: false,
      },
    ]);
  };

  const completeHabit = (habit: HabitType) => {
    const completion = !habit.complete;
    setHabits([
      ...localHabits.filter((oldHabit: HabitType) => oldHabit.id !== habit.id),
      {
        ...habit,
        complete: !habit.complete,
        history: [
          ...habit.history.filter(
            (his) => his.date !== new Date().toDateString()
          ),
          { date: new Date().toDateString(), complete: completion },
        ],
      },
    ]);
  };

  const deleteHabit = (habit: HabitType) =>
    setHabits([
      ...localHabits.filter((oldHabit: HabitType) => oldHabit.id !== habit.id),
    ]);

  return {
    all: localHabits,
    update: updateHabits,
    create: addNewHabit,
    complete: completeHabit,
    delHabit: deleteHabit,
  };
}

export default useHabits;
